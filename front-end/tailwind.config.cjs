/** @type {import('tailwindcss').Config} */
export default {
  plugins: [],
  theme: {
    extend: {},
    colors: {
      transparent: 'transparent',
      'primary-dark': '#274472',
      'primary-light': '#C3E0E5',
      'accent-dark': '#41729F',
      'accent-light': '#5885AF',
      'white': '#FFF',
      'black': '#000',
      'red': '#d45769',
      'green': '#35b265',
    },
  },
  purge: ["./index.html", './src/**/*.{svelte,js,ts}'], // for unused CSS
  variants: {
    extend: {},
  },
  darkMode: false, // or 'media' or 'class'
}
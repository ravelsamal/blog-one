export enum INPUT_TYPES {
	TEXT = 'text',
	EMAIL = 'email',
	NUMBER = 'number',
	PASSWORD = 'password'
}

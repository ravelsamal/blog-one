import { fetchBlog } from '$lib/BlogServices';
import { redirect, type LoadEvent } from '@sveltejs/kit';

export const load = async ({ params }: LoadEvent) => {
	try {
		const blog = await fetchBlog(params.id as string);
		return blog;
	} catch (error) {
    throw redirect(301, '/');
	}
};

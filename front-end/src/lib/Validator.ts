import type User from './interfaces/User';

export default class Validator {
	private first_name: string;
	private surname: string;
	private username: string;
	private email: string;
	private password: string;
	private repeat_password: string;

	constructor(user: User) {
		this.first_name = user.first_name;
		this.surname = user.surname;
		this.username = user.username;
		this.email = user.email;
		this.password = user.password;
		this.repeat_password = user.repeat_password;
	}

	validateFirstName = () => {
		if (!this.first_name) {
			throw new Error('First name is required.');
		}

		if (this.first_name.length < 3) {
			throw new Error('First name is too short.');
		}

		return null;
	};

	validateSurname = () => {
		if (!this.surname) {
			throw new Error('Surname is required.');
		}

		if (this.surname.length < 3) {
			throw new Error('Surname is too short.');
		}

		return null;
	};

	validateUsername = () => {
		if (!this.username) {
			throw new Error('Username is required.');
		}

		if (this.username.length < 3) {
			throw new Error('Username is too short.');
		}

		return null;
	};

	validateEmail = () => {
		if (!this.email) {
			throw new Error('Email is required.');
		}

		return null;
	};

	validatePassword = () => {
		if (this.password !== this.repeat_password) {
			throw new Error('Password does not match.');
		}

		if (this.password.length < 8) {
			throw new Error('Password is too short');
		}

		return null;
	};

	validateLogin = () => {
		this.validateEmail();
		this.repeat_password = this.password;
		this.validatePassword();

		return null;
	};

	validateRegisteration = () => {
		this.validateFirstName();
		this.validateSurname();
		this.validateUsername();
		this.validateEmail();
		this.validatePassword();

		return null;
	};
}

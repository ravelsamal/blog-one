import type Blog from './interfaces/Blog';

export const fetchBlogs = async (): Promise<Blog[]> => {
	const response: Response = await fetch('http://localhost:8000/api/blogs');
	const blogs = await response.json();

	return blogs;
};

export const fetchBlog = async (id: string): Promise<Blog> => {
  const url = `http://localhost:8000/api/blog/${id}`;
	const response: Response = await fetch(url);
  if(!response.ok) {
    throw new Error("Blog not found!");
  }
	const blog = (await response.json()).blog;

	return blog;
};

export const postBlog = async (data: Blog): Promise<Blog> => {
  const url = "http://localhost:8000/api/blog"
	const response: Response = await fetch(url, {
    method: "POST",
    credentials: "include",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json"
    },
  });

	const blog = await response.json();

	return blog;
};

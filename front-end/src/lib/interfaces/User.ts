export default interface User {
	first_name: string;
	surname: string;
	username: string;
	email: string;
	password: string;
	repeat_password: string;
}

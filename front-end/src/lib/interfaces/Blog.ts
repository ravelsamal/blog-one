export default interface Blog {
  id?: string
  title: string,
  body: string,
  thumbnail?: string,
  author?: string,
  createdAt?: Date;
  updatedAt?: Date;
}

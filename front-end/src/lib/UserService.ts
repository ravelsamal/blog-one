import { writable } from 'svelte/store';
import type User from './interfaces/User';
import Validator from './Validator';

export const isLoggedIn = writable(false);

export const register = async (data: User) => {
  const v = new Validator(data);
  v.validateRegisteration();

	const url = 'http://localhost:8000/api/register';
	const response = await fetch(url, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(data)
	});

  const responseBody = await response.json();

  if(!response.ok) {
    throw new Error(responseBody.message);
  }

	return responseBody
};

export const login = async (data: { email: string, password: string }) => {
  const v = new Validator({
    username: '',
    first_name: '',
    surname: '',
    email: data.email,
    password: data.password,
    repeat_password: data.password,
  });

  v.validateLogin();

	const url = 'http://localhost:8000/api/login';
	const response = await fetch(url, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
    credentials: "include",
		body: JSON.stringify(data)
	});

  const responseBody = (await response.json());

  if(!response.ok) {
    throw new Error(responseBody.message);
  }

  isLoggedIn.set(true);

	return responseBody
};

export const logout = async () => {
  const url = 'http://localhost:8000/api/logout';
  const response = await fetch(url, {
  	method: 'HEAD'
  });
  
  if (!response.ok) {
  	const responseBody = await response.json();
  	throw new Error(responseBody.message);
  }
  
  isLoggedIn.set(false);
};

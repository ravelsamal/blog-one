import express from "express"
import api from "./api/v1/api"
import mongoose from "mongoose"

const app = express()

app.use("/api/v1/user/", api)

mongoose.connect(<string>process.env.DB_URI)

app.listen(process.env.WEB_PORT, (error: Error | undefined) => {
  if(error) {
    console.log(error.message)
    return
  }

  console.log(`User service is listening on port ${process.env.WEB_PORT}`)
})

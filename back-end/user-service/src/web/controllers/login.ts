import express from "express"
import UserModel from "../../models/user"
import { issueTokenFor, Hash } from "../../utils/security"
import { validateLoginData } from "../../validations/user"

const login = async (req: express.Request, res: express.Response) => {
    try {
      const result = validateLoginData(req.body)
      if(result.length > 0) {
        res.status(400).json({
          error: {
            type: "Validation",
            detail: result
          }
        })
        return
      }

      const model = new UserModel()
      const user = await model.findByEmail(req.body.email)
      if(!user) {
        res.status(400).json({
          error: {
            type: "Credentials",
            detail: "Provided email does not belong to an account"
          }
        })
        return
      }

      const matches = Hash.verify(req.body.password, user.password)
      if(!matches) {
        res.status(400).json({
          error: {
            type: "Credentials",
            detail: "Wrong password"
          }
        })
        return
      }

      const token = await issueTokenFor({ username: user.username })
      const oneHourAge = 1000 * 3600
      res.cookie("token", token, { httpOnly: true, maxAge: oneHourAge, sameSite: "strict" })
      res.status(200).json({ user })
    } catch(error) {
      console.log(`Login controller: ${(<Error>error).message}`)
      res.status(500).json({
        error: {
          detail: "Internal server error"
        }
      })
    }

}

export default login

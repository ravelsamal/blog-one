import express from "express"
import UserModel from "../../models/user"
import { Hash } from "../../utils/security"
import { validateRegData } from "../../validations/user"

const register = async (req: express.Request, res: express.Response) => {
    try {
      req.body.firstName = req.body.first_name
      const result = validateRegData(req.body)
      if(result.length > 0) {
        res.status(400).json({
          error: {
            type: "Validation",
            detail: result
          }
        })
        return
      }

      req.body.password = await Hash.hash(req.body.password)
      const model = new UserModel()
      let found = await model.findByUsername(req.body.username)
      if(found) {
        res.status(400).json({
          error: {
            type: "Validation",
            detail: "Username is already in use."
          }
        })
        return
      }

      found = await model.findByEmail(req.body.email)
      if(found) {
        res.status(400).json({
          error: {
            type: "Validation",
            detail: "Email is already in use."
          }
        })
        return
      }

      
      res.status(201).json({
        user: model.register(req.body)
      })
    } catch(error) {
      console.log(`Register controller: ${(<Error>error).message}`)
      res.status(500).json({
        error: {
          detail: "Internal server error"
        }
      })
    }

}

export default register

import express from "express"
import register from "../../controllers/register"
import login from "../../controllers/login"

const v1API = express.Router()
v1API.use(express.json())
v1API.post("/register", register)
v1API.post("/login", login)

export default v1API

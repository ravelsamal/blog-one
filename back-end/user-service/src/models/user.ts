import mongoose from "mongoose";
import User from "../core/user";

export default class UserModel {
  private model = mongoose.model<User>("User", schema);
  async register(user: User): Promise<User> {
    return this.model.create(user)
  }

  async findByEmail(email: string): Promise<User | null> {
    return await this.model.findOne({email}, {_id: 0})
  }

  async findByUsername(username: string): Promise<User | null> {
    return await this.model.findOne({username}, {_id: 0})
  }
}

const schema = new mongoose.Schema({
  first_name: String,
  surname: String,
  username: {
    type: String,
    index: true
  },
  email: {
    type: String,
    index: true
  },
  password: String,
})

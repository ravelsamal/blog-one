import fs from "fs/promises"
import { hash as bcryptHash, compare } from "bcrypt"
import jwt from "jsonwebtoken"

export declare namespace Hash {
  function hash(data: string): Promise<string>
  function verify(data: string, hash: string): Promise<string>
}

export const hash = async (data: string): Promise<string> => {
  const rounds = 10
  return await bcryptHash(data, rounds)
}

export const verify = async (data: string, hash: string): Promise<boolean> => {
  return await compare(data, hash)
}

export const issueTokenFor = async (claims: any) => {
  const privateKey = await fs.readFile(<string>process.env.JWT_KEY_PRIVATE)
  const expireInOneHour = Math.floor(Date.now() / 1000) + 60 * 60
    return jwt.sign({
      keys: claims,
      exp: expireInOneHour
    }, privateKey, {algorithm: "RS256"})
}

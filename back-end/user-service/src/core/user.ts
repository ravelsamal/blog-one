export default interface User {
  firstName: string
  surname: string
  username: string
  email: string
  password: string
}

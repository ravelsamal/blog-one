export interface ValidationResult {
  valid: boolean
  property: string
  message: string
}

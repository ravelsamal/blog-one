import Joi from "joi"
import { ValidationResult } from "core/validation"

export function validateRegData(data: any): ValidationResult[] {
  return [
    validateFirstName(data.firstName),
    validateSurname(data.surname),
    validateUsername(data.username),
    validateEmail(data.email),
    validatePassword(data.password)
  ].filter(result => !result.valid)
}

export function validateLoginData(data: any): ValidationResult[] {
  return [
    validateEmail(data.email),
    validatePassword(data.password)
  ].filter(result => !result.valid)
}

export function validateFirstName(name: string): ValidationResult {
  const property = "First name"
  const result = Joi.string()
  .required()
  .min(3)
  .max(30)
  .label(property)
  .validate(name)

  if(result.error?.message) {
    return {
      valid: false,
      property,
      message: result.error.message
    }
  }

  return { valid: true, property, message: "" }
}

export function validateSurname(name: string): ValidationResult {
  const property = "Surname"
  const result = Joi.string()
  .required()
  .min(3)
  .max(30)
  .label(property)
  .validate(name)

  if(result.error?.message) {
    return {
      valid: false,
      property,
      message: result.error.message
    }
  }

  return { valid: true, property, message: "" }
}

export function validateUsername(username: string): ValidationResult {
  const property = "Username"
  const result = Joi.string()
  .required()
  .min(3)
  .max(30)
  .alphanum()
  .label(property)
  .validate(username)

  if(result.error?.message) {
    return {
      valid: false,
      property,
      message: result.error.message
    }
  }

  return { valid: true, property, message: "" }
}

export function validateEmail(email: string): ValidationResult {
  const property = "Email"
  const result = Joi.string()
  .required()
  .email({ minDomainSegments: 2 })
  .label(property)
  .validate(email)

  if(result.error?.message) {
    return {
      valid: false,
      property,
      message: result.error.message
    }
  }

  return { valid: true, property, message: "" }
}

export function validatePassword(password: string): ValidationResult {
  const property = "Password"
  const result = Joi.string()
  .required()
  .min(8)
  .max(60)
  .label(property)
  .validate(password)

  if(result.error?.message) {
    return {
      valid: false,
      property,
      message: result.error.message
    }
  }

  return { valid: true, property, message: "" }
}

import Blog from "../core/blog"
import fs from "fs/promises"
import jwt, { JwtPayload } from "jsonwebtoken"
import xss from "xss"

export const verifyToken = async (token: string) => {
  const publicKey = await fs.readFile(<string>process.env.JWT_KEY_PUBLIC)
  return (<JwtPayload>jwt.verify(token, publicKey)).keys
}

export const sanitizeBlog = (blog: Blog): Blog => {
  return Object.assign(blog, {
    title: xss(blog.title),
    body: xss(blog.body)
  })
}

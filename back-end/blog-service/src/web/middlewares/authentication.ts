import express from "express"
import { verifyToken } from "../../utils/security"

async function authenticate(req: express.Request, res: express.Response, next: express.NextFunction) {
  let token: string = req.cookies.token!
  if (!token) {
    res.status(401).json({
      message: "Unauthorized",
    })
    return
  }

  try {
    req.body.author =  await verifyToken(token)
  } catch (error) {
    res.status(401).json({
      message: "Unauthorized",
    })
    return
  }

  next()
}

export default authenticate

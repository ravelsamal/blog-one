import express from "express"
import { validateBlogData } from "../../validations/blog"
import Blog from "../../core/blog"
import { sanitizeBlog } from "../../utils/security"
import BlogModel from "../../models/blog"

async function create(req: express.Request, res: express.Response) {
    const result = validateBlogData(req.body)
    if(result.length > 0) {
      res.status(400).json({
        error: {
          type: "Validation",
          detail: result
        },
      })
      return
    }

    try {
      const blog: Blog = Object.assign(sanitizeBlog(req.body), {
        author: req.body.author.username,
      })

      await new BlogModel().create(blog)
      res.status(201).json({
        message: "Blog successfully created"
      })

    } catch(error) {
      console.log(`Create controller: ${(<Error>error).message}`)
      res.status(500).json({
        error: {
          type: "Server",
          detail: "Internal server error"
        }
      })
      return
    }
}
export default create

import express from "express"
import BlogModel from "../../models/blog"
import { validateBlogID } from "validations/blog"

async function blog(req: express.Request, res: express.Response) {
  try {
    if(!validateBlogID(req.params.id).valid) {
      res.status(400).json({
        error: {
          type: "Validation",
          detail: validateBlogID(req.params.id).message
        }
      })
      return
    }

    const model = new BlogModel()
    const blog = await model.findByID(req.params.id)
    if(!blog) {
      res.status(404).json({
        error: {
          type: "Bad URL",
          detail: "Blog not found."
        },
      })
      return
    }

    res.status(200).json({ blog })
  } catch (error) {
    res.status(500).json({
      error: {
        detail: "Internal server error"
      }

    })
  }
}

export default blog

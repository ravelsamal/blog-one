import express from "express"
import BlogModel from "../../models/blog"

async function blogs(req: express.Request, res: express.Response) {
  try {
    res.status(200).json({
      blogs: await new BlogModel().getBlogs()
    })
  } catch (error) {
    res.status(500).json({
      error: {
        detail: "Internal server error"
      }

    })
  }
}

export default blogs

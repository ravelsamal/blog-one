import express from "express"
import create from "../../controllers/create"
import authenticate from "../../middlewares/authentication"
import blogs from "../../controllers/blogs"
import blog from "../../controllers/blog"

const v1API = express.Router()
v1API.use(express.json())
v1API.post("/create", authenticate, create)
v1API.get("/blogs", blogs)
v1API.get("/blogs/:id", blog)

export default v1API

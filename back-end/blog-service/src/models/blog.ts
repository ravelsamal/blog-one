import { Schema, model } from "mongoose"
import Blog from "../core/blog"

export default class BlogModel {
  private model = model<Blog>("Blog", schema)

  async create(blog: Blog): Promise<Blog> {
    return this.model.create(blog)
  }

  async findByID(id: string): Promise<Blog | null> {
    return await this.model.findById(id)
  }

  async getBlogs(): Promise<Blog[]> {
    return await this.model.aggregate([
      {$sort: {"createdAt": -1}},
      {$project: {title: 1, thumbnail: 1}}
    ])
  }
}

const schema = new Schema<Blog>({
  title: String,
  body: String,
  thumbnail: String,
  author: String,
  createdAt: Date,
  updatedAt: Date,
})

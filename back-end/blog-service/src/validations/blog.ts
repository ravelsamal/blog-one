import Joi from "joi"
import Blog from "../core/blog"
import ValidationResult from "../core/validation"
import mongoose from "mongoose"

export function validateBlogData(blog: Blog): ValidationResult[] {
  return [
    validateBlogTitle(blog.title),
    validateBlogBody(blog.body)
  ].filter(result => !result.valid)
}

export function validateBlogTitle(title: string): ValidationResult {
  const property = "Title"
  const result = Joi.string()
    .required()
    .min(5)
    .max(200)
    .label(property)
    .validate(title)

    if(result.error?.message) {
      return {
        valid: false,
        property: property,
        message: result.error.message
      }
    }

  return { valid: true, property: property, message: "" }
}

export function validateBlogBody(body: string): ValidationResult {
  const property = "Body"
  const result = Joi.string()
    .required()
    .min(5)
    .label(property)
    .validate(body)

    if(result.error?.message) {
      return {
        valid: false,
        property: property,
        message: result.error.message
      }
    }

  return { valid: true, property: property, message: "" }
}

export function validateBlogID(id: string): ValidationResult {
  const property = "ID"
    if(mongoose.isValidObjectId(id)) {
      return {
        valid: false,
        property: property,
        message: "Invalid ObjectID"
      }
    }

  return { valid: true, property: property, message: "" }
}

export default interface Blog {
  title: string
  body: string
  thumbnail: string
  author: string
  createdAt: Date
  updatedAt: Date
}

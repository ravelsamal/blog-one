export default interface ValidationResult {
  valid: boolean
  property: string
  message: string
}

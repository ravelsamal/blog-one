backend_dev_start:
	@echo "Starting back-end service..."
	cd back-end && npm run dev
	@echo "Done."

containers_up:
	@echo "Starting mongodb container..."
	docker compose up -d
	@echo "Done."

containers_down:
	@echo "Starting mongodb container..."
	docker compose down
	@echo "Done."


start_dev:
	@echo "Starting mongodb container..."
	docker compose up -d
	@echo "Done."

	@echo "Starting back-end service..."
	cd back-end && npm run dev
	@echo "Done."